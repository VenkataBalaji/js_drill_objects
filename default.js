//function defaults(obj, defaultProps) {
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
//}



function defaults(obj,defaultProps){
   for (const key in defaultProps) {
    obj[key] = defaultProps[key];
   }
   return obj;
}


module.exports=defaults;